# msc_project

Masters Group Project 


## Installation

### Clone the repository 

```bash 
# clone with HTTPS
git clone https://gitlab.com/juve2018/msc_project.git msc_project 
# or optionally clone with SSH
# git clone git@gitlab.com:juve2018/msc_project.git msc_project
# go to the project directory using cd
cd msc_project
```

### Create virtualenv and install requirements 
```bash 
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt
```

## Running Data Pre-processing Script

```bash 
python3 codes/process_data.py
```

## Running Model Training Script 

### Run Random Forest Model training Script
```bash 
python3 codes/ml_classification/basic_random_forest.py
```

### Run Support Vector Machines (SVM) Model training Script
```bash 
python3 codes/ml_classification/basic_random_forest.py
```


## Running Visualization Script

```bash
python3 codes/visulization/explainer_dashboard.py
```
