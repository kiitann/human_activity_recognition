
type = ['', 'info', 'success', 'warning', 'danger'];

if(todaypoints.length <= 0){
	todaypoints = ['','','','','','','',''];
	todaycats = ['3am', '6am', '9am','12am', '3pm', '6pm', '9pm', '12pm'];
}

if(thisWeekPoints.length <= 0){
	thisWeekPoints = ['','','','','','',''];
	thisWeekCategories = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
}

if(thismoncats.length <=0){
	var totalDays = 31;
	for(x=1;x<=totalDays;x++){
		thismoncats.push(x);
		thismoncolls.push('');
	}
}

dashboardcharts = {
    initDashboardPageCharts: function() {
    	
    	/* ----------==========   This Month Collections Chart    ==========---------- */
		var thisMonthOptions = {
				chart: {type: 'spline',height: 350,marginBottom: 90},
				title : {text: '',x : -20},
				subtitle : {text : '',x : -20},
				xAxis : {
					title : {text : 'DAY'},
				},
				yAxis : {
					title : {text : 'Collections (Tzs)'},
					plotLines : [ {value : 0,width : 1,color : '#808080'} ],
				},
				plotOptions: {
			        series: {
			        	animation: {
			                duration: 2000
			            },
			        	lineWidth: 3,
			            marker: {
			                fillColor: '#FFFFFF',
			                lineWidth: 3,
			                lineColor: null 
			            }
			        }
			    },
				tooltip : {
					valueSuffix : ' Tzs'
				},
				legend : {
    				layout : 'vertical',
    				align : 'center',
    				verticalAlign : 'bottom',
    				borderWidth : 0,
    				itemStyle: {
    	                 font: '9pt Trebuchet MS, Verdana, sans-serif'
    	              },
    	              itemHoverStyle: {
    	            	  color: '#A0A0A0'
    	              }
    			},
				series : [
					/*{name : 'Total Collections'}*/
				]
			};

			thisMonthOptions.chart.renderTo = 'performanceline';
			thisMonthOptions.xAxis.categories = thismoncats;
			//thisMonthOptions.series[0].data = thismoncolls;
			thisMonthOptions.series = thismonseries;
			var thisMonthChart = new Highcharts.Chart(thisMonthOptions);
		
    	
    	/* ----------==========   Todays Collections Chart    ==========---------- */
    	var todayOptions = {
    			chart: {type: 'spline',height: 170},
    			title : {text: '',x : -20},
    			subtitle : {text : '',x : -20},
    			xAxis : {
    				lineWidth: 0,
    				lineColor: 'transparent',
    				tickWidth: 0,
    				gridLineDashStyle: 'dot',
    				minorGridLineWidth:0,
    				labels: {style: {color: '#FFFFFF'}},
    				categories :[]
    			},
    			
    			yAxis : {title : {text : ''},
    				
    				labels: {
    		            style: {
    		                color: '#FFFFFF'
    		            }
    		        },
    				gridLineDashStyle: 'dot',
    				minorGridLineWidth:0,
    				plotLines : [ {
    					value : 0,
    					width : 0,
    					color : '#808080'
    				} ],
    			},
    			plotOptions: {
    		        series: {
    		        	animation: {
    		                duration: 2000
    		            },
    		        	lineWidth: 3,
    		        	lineColor: '#FFFFFF',
    		            marker: {
    		                fillColor: '#FFFFFF',
    		                lineWidth: 0,
    		                lineColor: null,
    		                states: {
    		                    hover: {
    		                        fillColor: '#FFFFFF',
    		                        lineColor: '#FFFFFF',
    		                        lineWidth: 1
    		                    }
    		                }
    		            }
    		        }
    		    },
    			tooltip : {
    				pointFormat: "<b>{point.y:,.0f}</b> Tzs"
    			},
    			exporting: {enabled: false },
    			series : [{
    					name : '',
    					showInLegend: false, 
    					data :[]
    			}]
    		};
    		
    		todayOptions.chart.renderTo = 'todayChart';
    		todayOptions.xAxis.categories = todaycats;
    		todayOptions.series[0].data = todaypoints;
    		var todayChart = new Highcharts.Chart(todayOptions);
    	
    		
    	/* ----------==========   This Week Collections Chart    ==========---------- */
    	var weeklyOptions = {
    			chart: {type: 'spline',height: 170},
    			title : {text: '',x : -20},
    			subtitle : {text : '',x : -20},
    			xAxis : {
    				lineWidth: 0,
    				lineColor: 'transparent',
    				tickWidth: 0,
    				gridLineDashStyle: 'dot',
    				minorGridLineWidth:0,
    				labels: {style: {color: '#FFFFFF'}},
    				categories :[]
    			},
    			
    			yAxis : {title : {text : ''},
    				labels: {
    		            style: {
    		                color: '#FFFFFF'
    		            }
    		        },
    				gridLineDashStyle: 'dot',
    				minorGridLineWidth:0,
    				plotLines : [ {
    					value : 0,
    					width : 0,
    					color : '#808080'
    				} ],
    			},
    			plotOptions: {
    		        series: {
    		        	animation: {
    		                duration: 2000
    		            },
    		        	lineWidth: 3,
    		        	lineColor: '#FFFFFF',
    		            marker: {
    		                fillColor: '#FFFFFF',
    		                lineWidth: 0,
    		                lineColor: null,
    		                states: {
    		                    hover: {
    		                        fillColor: '#FFFFFF',
    		                        lineColor: '#FFFFFF',
    		                        lineWidth: 1
    		                    }
    		                }
    		            }
    		        }
    		    },
    			tooltip : {
    				pointFormat: "<b>{point.y:,.0f}</b> Tzs"
    			},
    			exporting: {enabled: false },
    			series : [{
    					name : '',
    					showInLegend: false, 
    					data : []
    			}]
    		};
    		
    		weeklyOptions.chart.renderTo = 'thisWeekCollectionsChart';
    		weeklyOptions.xAxis.categories = thisWeekCategories;
    		weeklyOptions.series[0].data = thisWeekPoints;
    		var weeklyChart = new Highcharts.Chart(weeklyOptions);
    		
    		
            /* ----------==========   This Year Monthly Collections Chart    ==========---------- */
        	var monthlyOptions = {
        			chart: {type: 'column',height: 170},
        			title : {text: '',x : -20},
        			subtitle : {text : '',x : -20},
        			xAxis : {
        				lineWidth: 0,
        				lineColor: 'transparent',
        				tickWidth: 0,
        				gridLineDashStyle: 'dot',
        				minorGridLineWidth:0,
        				labels: {style: {color: '#FFFFFF'}},
        				categories :[]
        			},
        			yAxis : {title : {text : ''},
        				labels: {style: {color: '#FFFFFF'}},
        				gridLineDashStyle: 'dot',
        				minorGridLineWidth:0,
        				plotLines : [ {value : 0,width : 0,color : '#808080'} ],
        			},
        			plotOptions: {
        				 series: {animation: {duration: 2000},
        			            borderRadius: 2,
        			            borderColor: '#FFFFFF',
        			            color: '#e3dfe3'
        			        }
        		    },
        			tooltip : {pointFormat: "<b>{point.y:,.0f}</b> Tzs"},
        			exporting: {enabled: false },
        			series : [{name : '',showInLegend: false,data :[]}]
        		};
        		monthlyOptions.chart.renderTo = 'montlyCollectionsChart';
        		monthlyOptions.xAxis.categories = thisYearCategories;
        		monthlyOptions.series[0].data = thisYearPoints;
        		var monthlyChart = new Highcharts.Chart(monthlyOptions);
        		
        		
        	/* ----------==========   This Year Psp Collections Line Chart    ==========----------*/ 
        		/*var pspThisYearOptions = {
        			chart: {type: 'column',height: 350,marginBottom: 30},
        			title : {text: '',x : -20},
        			subtitle : {text : '',x : -20},
        			xAxis : {title : {text : 'MONTH'},categories :[],crosshair: true},
        			yAxis : {title : {text : 'Collections (Tzs)'},
        				plotLines : [ {value : 0,width : 1,color : '#808080'} ],
        			},
        			plotOptions: {
        				column: {
        		            pointPadding: 0.2,
        		            borderWidth: 0
        		        },
        		        series: {
        		        	animation: {duration: 2000},
        		        	lineWidth: 3,
        		            marker: {fillColor: '#FFFFFF',lineWidth: 3,
        		                lineColor: null}
        		            ,stacking: 'normal'
        		        }
        		    },
        			tooltip : {valueSuffix : ' Tzs'},
        			tooltip: {
        		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: <b>{point.percentage:.1f}%</b></td>' +
        		            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        		        footerFormat: '</table>',
        		        shared: true,
        		        useHTML: true
        		    },
        		    legend :{
      	              itemHoverStyle: {
      	            	  color: '#A0A0A0'
      	              }
      			},
        			legend : {
        				layout : 'vertical',
        				align : 'center',
        				verticalAlign : 'bottom',
        				borderWidth : 0,
        				itemStyle: {
        	                 font: '8pt Trebuchet MS, Verdana, sans-serif'
        	              },
        	              itemHoverStyle: {
        	            	  color: '#A0A0A0'
        	              }
        				},
        			series : []
        		};
        		
        		pspThisYearOptions.chart.renderTo = 'pspperformanceline';
        		pspThisYearOptions.xAxis.categories = pspdatamonths;
        		pspThisYearOptions.series = psplineseries;
        		var pspYearChart = new Highcharts.Chart(pspThisYearOptions);*/
        		
        		
        		/*var pspThisMonthPieOptions = {
            		    chart: {type: 'pie',height: 350,
            		        options3d: {enabled: true,alpha: 30,beta:0},
            				events: {
            			        load: function () {
            			          this.series[0].data[0].slice();
            			        }
            			      }
            			    },
            		    title: {text: ''},
            		    subtitle: {text: ''},
            		    plotOptions: {
            		        pie: {innerSize: 150,depth: 20,allowPointSelect: true,slicedOffset: 25,cursor: 'pointer',
            		            dataLabels: {enabled: true,format: '{point.percentage:.1f} %',
            		                style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}
            		            },
            					showInLegend: true
            		        }
            		    },
            		    legend :{
            	              itemHoverStyle: {
            	            	  color: '#A0A0A0'
            	              }
            			},
            		    series: [{name: 'Amount Collected',data:[] }] 
            		};
        			pspThisMonthPieOptions.chart.renderTo = 'pspperformanceline';
        			pspThisMonthPieOptions.series[0].data = pspdonutseriesdata;
            		var pspYearChart = new Highcharts.Chart(pspThisMonthPieOptions);*/
        		
            	
        		
        	/* ----------==========   This Year Psp Collections Donut Chart    ==========---------- */
        		var pspThisYearPieOptions = {
        		    chart: {type: 'pie',height: 350,
        		        options3d: {enabled: true,alpha: 30,beta:0},
        				events: {
        			        load: function () {
        			          this.series[0].data[0].slice();
        			        }
        			      }
        			    },
        		    title: {text: ''},
        		    subtitle: {text: ''},
        		    plotOptions: {
        		        pie: {innerSize: 150,depth: 20,allowPointSelect: true,slicedOffset: 25,cursor: 'pointer',
        		            dataLabels: {enabled: true,format: '{point.percentage:.1f} %',
        		                style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'}
        		            },
        					showInLegend: true
        		        }
        		    },
        		    legend :{
        	              itemHoverStyle: {
        	            	  color: '#A0A0A0'
        	              }
        			},
        		    series: [{name: 'Amount Collected',data:[] }] 
        		};
        		pspThisYearPieOptions.chart.renderTo = 'pspthisyearpie';
        		pspThisYearPieOptions.series[0].data = pspdonutseriesdata;
        		var pspYearChart = new Highcharts.Chart(pspThisYearPieOptions);
        		
    }
}