$(document).ready(function () {
  $("#myTable").DataTable();

  //all Time Modal PopUp

  Highcharts.chart("modelAllTimeDiv", {
    chart: {
      type: "column",
    },

    title: {
      text: "Total fruit consumption, grouped by gender",
    },

    xAxis: {
      categories: ["Apples", "Oranges", "Pears", "Grapes", "Bananas"],
    },

    yAxis: {
      allowDecimals: false,
      min: 0,
      title: {
        text: "Number of fruits",
      },
    },

    tooltip: {
      formatter: function () {
        return (
          "<b>" +
          this.x +
          "</b><br/>" +
          this.series.name +
          ": " +
          this.y +
          "<br/>" +
          "Total: " +
          this.point.stackTotal
        );
      },
    },

    plotOptions: {
      column: {
        stacking: "normal",
      },
    },

    series: [
      {
        name: "John",
        data: [5, 3, 4, 7, 2],
        stack: "male",
      },
      {
        name: "Joe",
        data: [3, 4, 4, 2, 5],
        stack: "male",
      },
      {
        name: "Jane",
        data: [2, 5, 6, 2, 1],
        stack: "female",
      },
      {
        name: "Janet",
        data: [3, 0, 4, 4, 3],
        stack: "female",
      },
    ],
  });

  let options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  let date = new Date();
  date = date.toLocaleDateString("en-US", options);

  console.log(date);
  let totalTimeDownStairs = 12000;
  let totalCollectionsThisYear = 13000;
  let totalCollThisMonth = 140000;
  let totalToday = 240000;

  //Set Summary Values
  $("#totalTimeDownStairs").html(totalTimeDownStairs);
  $("#modalTimeDownStairs").html(totalTimeDownStairs);
  $("#totalCollectionsThisYear").html(totalCollectionsThisYear);
  $("#modalYearTotal").html(totalCollectionsThisYear);
  $("#totalCollThisMonth").html(totalCollThisMonth);
  $("#modalMonthTotal").html(totalCollThisMonth);
  $("#totalCollToday").html(totalToday);
  $("#modalTodayTotal").html(totalToday);
  $("#todayDate").html(date);

  // random live chart one
  Highcharts.chart("montlyPerfomanceChart", {
    chart: {
      type: "spline",
      animation: Highcharts.svg, // don't animate in old IE
      marginRight: 10,
      events: {
        load: function () {
          // set up the updating of the chart each second
          var series = this.series[0];
          setInterval(function () {
            var x = new Date().getTime(), // current time
              y = Math.random();
            series.addPoint([x, y], true, true);
          }, 1000);
        },
      },
    },

    time: {
      useUTC: false,
    },

    title: {
      text: "",
    },

    accessibility: {
      announceNewData: {
        enabled: true,
        minAnnounceInterval: 15000,
        announcementFormatter: function (allSeries, newSeries, newPoint) {
          if (newPoint) {
            return "New point added. Value: " + newPoint.y;
          }
          return false;
        },
      },
    },

    xAxis: {
      type: "datetime",
      tickPixelInterval: 150,
    },

    yAxis: {
      title: {
        text: "Value",
      },
      plotLines: [
        {
          value: 0,
          width: 1,
          color: "#808080",
        },
      ],
    },

    tooltip: {
      headerFormat: "<b>{series.name}</b><br/>",
      pointFormat: "{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}",
    },

    legend: {
      enabled: false,
    },

    exporting: {
      enabled: false,
    },

    series: [
      {
        name: "Random data",
        data: (function () {
          // generate an array of random data
          var data = [],
            time = new Date().getTime(),
            i;

          for (i = -19; i <= 0; i += 1) {
            data.push({
              x: time + i * 1000,
              y: Math.random(),
            });
          }
          return data;
        })(),
      },
    ],
  });

  // bar chart
  Highcharts.chart("performance", {
    chart: {
      type: "bar",
    },
    title: {
      text: "",
    },
    xAxis: {
      categories: [
        "John Doglous",
        "Herieth Peter",
        "Othman Amin",
        "Rick Oneal",
        "Tomas Heritier",
      ],
      title: {
        text: null,
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Time (Mins)",
        align: "high",
      },
      labels: {
        overflow: "justify",
      },
    },
    tooltip: {
      valueSuffix: " millions",
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true,
        },
      },
    },
    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "top",
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "#FFFFFF",
      shadow: true,
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        name: "upstairs",
        data: [107, 31, 635, 203, 2],
      },
      {
        name: "up_podium",
        data: [133, 156, 947, 408, 6],
      },
      {
        name: "down_podium",
        data: [814, 841, 3714, 727, 31],
      },
      {
        name: "up_hoist",
        data: [1216, 1001, 4436, 738, 40],
      },
    ],
  });

  // diagram for pie chart
  Highcharts.chart("servicedonut", {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: "pie",
    },
    title: {
      text: "",
    },
    tooltip: {
      pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
    },
    accessibility: {
      point: {
        valueSuffix: "%",
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: "pointer",
        dataLabels: {
          enabled: true,
          format: "<b>{point.name}</b>: {point.percentage:.1f} %",
        },
      },
    },
    series: [
      {
        name: "Brands",
        colorByPoint: true,
        data: [
          {
            name: "Upstairs",
            y: 61.41,
            sliced: true,
            selected: true,
          },
          {
            name: "Up_podium",
            y: 11.84,
          },
          {
            name: "Down_podium",
            y: 20.85,
          },
          {
            name: "Up_hoist",
            y: 5.67,
          },
          {
            name: "Down_hoist",
            y: 5.67,
          },
        ],
      },
    ],
  });
});

//pie chart
