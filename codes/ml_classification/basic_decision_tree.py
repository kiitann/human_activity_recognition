#!/usr/bin/env python3
import os
import glob
import time
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, plot_confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score


# Classification algorithm we can use [scikit-learn]:
# SVC, DecisionTreeClassifier, GaussianNB, MLPClassifier, RandomForestClassifier

DIR = os.path.dirname(
    os.path.abspath(__file__)
)

LABELED_DATA_DIR = os.path.join(
    DIR,
    "../raw_data"
)


data_frames = []
for labeled_file in glob.glob(
        os.path.join(LABELED_DATA_DIR, "*_labeled.csv")):
    print(labeled_file)
    # if ("210708_d8bfc00374ab" in labeled_file) or ("210708_d8bfc0037339" in labeled_file):
    # if "210708_d8bfc0037339" in labeled_file:
    df = pd.read_csv(labeled_file)
    data_frames.append(df)

main_df = pd.concat(data_frames)
main_df.drop_duplicates(inplace=True)
features = ['acc_mag', 'gyro_mag', 'pressure']
data = main_df[features]

scaler = MinMaxScaler()
arr = scaler.fit_transform(data)
df_norm = pd.DataFrame(arr, columns=features)
print(df_norm.head())
x = df_norm
y = main_df['activity_class']
X_train, X_test, y_train, y_test = train_test_split(
    x, y, test_size=0.33, random_state=42
)

X_test.to_csv(
    os.path.join(
        DIR,
        "X_test_decision_tree.csv"
    )
)
y_test.to_csv(
    os.path.join(
        DIR,
        "y_test_decision_tree.csv"
    )
)
t1 = time.time()
clf = DecisionTreeClassifier(max_depth=4, random_state=0)
clf.fit(X_train, y_train)
print(f"training time: {time.time() - t1}")
joblib.dump(
    clf,
    os.path.join(
        DIR,
        "joblib_decision_tree.joblib"
    ),
    compress=9
)
y_pred = clf.predict(X_test)
activity_class = {
    'no_change',
    'upstairs',
    'up_podium',
    'down_podium',
    'downstairs',
    'down_hoist',
    'up_hoist'
}
plot_confusion_matrix(clf, X_test, y_test, display_labels=activity_class)
plt.show()
y_pred_p = np.argmax(y_pred, axis=-1)
y_true_p = np.argmax(y_test, axis=-1)
cm = confusion_matrix(y_test, y_pred)
# Accuracy, Recall, Precision scores
acc = accuracy_score(y_test, y_pred)
rec = recall_score(y_test, y_pred, average='macro')
prec = precision_score(y_test, y_pred, average='macro')
f1 = f1_score(y_test, y_pred, average='macro')

# Print Scores
rounding = 2
print(
    ' Scores\n',
    '-'*21,
    f'\n Accuracy:\t{round(acc, rounding)}'
    f'\n Recall:\t{round(rec, rounding)}'
    f'\n Precision:\t{round(prec, rounding)}'
    f'\n F-Score:\t{round(f1, rounding)}'
)
