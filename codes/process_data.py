#!/usr/bin/env python3
import os
import math
import shutil
import pandas as pd

DIR = os.path.dirname(os.path.abspath(__file__))

RAW_DATA_ZIP = os.path.join(
    DIR,
    "raw_data.zip"
)

TRANSPORT_LABELS = os.path.join(
    DIR,
    "transport_labels.xlsx"
)

EXTRACT_DIR = os.path.join(
    DIR,
    "raw_data"
)

if not os.path.isdir(EXTRACT_DIR):
    # creating the `EXTRACT_DIR` if not exists
    # if directory creation fail then `EXTRACT_DIR`
    # set to `None`
    try:
        os.makedirs(EXTRACT_DIR, exist_ok=True)
    except OSError:
        EXTRACT_DIR = None

transport_labels = pd.read_excel(
    TRANSPORT_LABELS
)

# unzip raw_data.zip
shutil.unpack_archive(
    os.path.join(
        DIR,
        RAW_DATA_ZIP
    ),
    EXTRACT_DIR
)

sessions = transport_labels.session.unique()
activity = transport_labels.activity.unique()

activity_class = {
    'no_change': 0,
    'upstairs': 1,
    'up_podium': 2,
    'down_podium': 3,
    'downstairs': 4,
    'down_hoist': 5,
    'up_hoist': 6
}

print(f"no of persons observed: {len(sessions)}: {sessions}")
print(f"activity tracked: {len(activity)}: {activity}\n")

for session in sessions:
    print(f"\n--------------")
    df = pd.read_csv(
        os.path.join(
            EXTRACT_DIR,
            f"{session}_raw.csv"
        )
    )

    df['dt'] = df['dt'].astype('datetime64[ns]')
    df['activity'] = ""
    df.index = df['dt']
    for i, label in transport_labels.iterrows():
        df.loc[label.start_time: label.end_time, "activity"] = label.activity

    total_rows = df.shape[0]
    df = df.loc[df['activity'] != ""]
    # calculate the magnitude of accelerometer reading
    df['acc_mag'] = df.apply(
        lambda row: math.sqrt(
            row.acc_x ** 2 + row.acc_y ** 2 + row.acc_z ** 2
        ),
        axis=1
    )
    # calculate the magnitude of gyroscope reading
    df['gyro_mag'] = df.apply(
        lambda row: math.sqrt(
            row.gyro_x ** 2 + row.gyro_y ** 2 + row.gyro_z ** 2
        ),
        axis=1
    )
    # applying numeric class for activities
    df['activity_class'] = df.apply(
        lambda row: activity_class[row.activity],
        axis=1
    )
    labeled_rows = df.shape[0]
    df = df.dropna()
    cleaned_rows = df.shape[0]
    activity = df['activity'].unique()
    print(f"session: {session}")
    print(f"total_rows: {total_rows} :: labeled_rows: {labeled_rows} "
          f":: cleaned_rows: {cleaned_rows}")
    print(f"activity: {activity}")
    df.to_csv(
        os.path.join(
            EXTRACT_DIR,
            f"{session}_labeled.csv"
        ),
        index=False
    )
