#!/usr/bin/env python3
import os
import joblib
import pandas as pd
from explainerdashboard import ClassifierExplainer, ExplainerDashboard


DIR = os.path.dirname(
    os.path.abspath(__file__)
)

MODEL_DIR = os.path.join(
    DIR,
    "../ml_classification"
)
print(
    os.path.join(
        MODEL_DIR,
        "joblib_random_forest.joblib"
    )
)
random_forest_model = joblib.load(
    os.path.join(
        MODEL_DIR,
        "joblib_random_forest.joblib"
    )
)

X_test = pd.read_csv(
    os.path.join(
        MODEL_DIR,
        "X_test_random_forest.csv"
    )
)
y_test = pd.read_csv(
    os.path.join(
        MODEL_DIR,
        "y_test_random_forest.csv"
    )
)

explainer = ClassifierExplainer(
    random_forest_model,
    X_test[['acc_mag', 'gyro_mag', 'pressure']],
    y_test['activity_class'],
    labels=[
        'no_change',
        'upstairs',
        'up_podium',
        'down_podium',
        'downstairs',
        'down_hoist',
        'up_hoist'
    ]
)

ExplainerDashboard(
    explainer,
    whatif=False,
    shap_interaction=False,
    decision_trees=False
).run(port=8051)
