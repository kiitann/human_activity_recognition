import os
import gantt
import datetime
from yaml import load
from datetime import date
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


DIR = os.path.dirname(
    os.path.abspath(__file__)
)

gantt.define_font_attributes(
    fill='black', stroke='black', stroke_width=0,
    font_family="Verdana"
)


with open(os.path.join(DIR, "project_activity_plan.yaml")) as fh:
    project = load(fh.read(), Loader=Loader)


all_projects = gantt.Project(name=project["name"])
project_resources = {}
for main_task in project["tasks"]:
    main_task_obj = gantt.Project(name=main_task["name"])
    for sub_task in main_task["sub_tasks"]:
        resources = []
        for resource in sub_task["resources"]:
            resource_obj = project_resources.get(resource)
            if resource_obj is None:
                resource_obj = gantt.Resource(resource)
                project_resources[resource] = resource_obj
            resources.append(resource_obj)
        sub_task_obj = gantt.Task(
            name=sub_task["name"],
            start=datetime.datetime.strptime(
                sub_task["date"],
                "%Y-%m-%d"
            ).date(),
            duration=sub_task["duration"],
            resources=resources,
            color=sub_task["color"]
        )
        main_task_obj.add_task(sub_task_obj)
    all_projects.add_task(main_task_obj)


all_projects.make_svg_for_tasks(
    filename='project_gantt_chart.svg',
    today=datetime.datetime.today().date(),
    start=date(2022, 1, 17),
    end=date(2022, 4, 1)
)
